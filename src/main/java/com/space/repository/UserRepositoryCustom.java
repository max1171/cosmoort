package com.space.repository;

import com.space.model.Ship;
import com.space.model.ShipType;

import java.util.List;

public interface UserRepositoryCustom {
    List<Ship> findFilterAndPaginationShip(Integer pageNumber, Integer pageSize, String order, String name, String planet, ShipType shipType, Long after, Long before, Boolean isUsed, Double minSpeed,
                                           Double maxSpeed, Integer minCrewSize, Integer maxCrewSize, Double minRating, Double maxRating);
    List<Ship> findFilterShip( String order,String name, String planet, ShipType shipType, Long after, Long before, Boolean isUsed, Double minSpeed,
                                               Double maxSpeed, Integer minCrewSize, Integer maxCrewSize, Double minRating, Double maxRating);
}
