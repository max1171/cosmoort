package com.space.repository;

import com.space.model.Ship;
import com.space.model.ShipType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class UserRepositoryCustomImpl implements UserRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public List<Ship> findFilterAndPaginationShip(
            final Integer pageNumber,
            final Integer pageSize,
            final String order,
            final String name,
            final String planet,
            final ShipType shipType,
            final Long after,
            final Long before,
            final Boolean isUsed,
            final Double minSpeed,
            final Double maxSpeed,
            final Integer minCrewSize,
            final Integer maxCrewSize,
            final Double minRating,
            final Double maxRating
    ) {
        return entityManager.createQuery(createFilter(order, name, planet, shipType, after, before, isUsed, minSpeed, maxSpeed, minCrewSize, maxCrewSize, minRating, maxRating)
                                            ).setFirstResult(pageNumber * pageSize).setMaxResults(pageSize).getResultList();
    }
    @Override
    public List<Ship> findFilterShip(
            final String order,
            final String name,
            final String planet,
            final ShipType shipType,
            final Long after,
            final Long before,
            final Boolean isUsed,
            final Double minSpeed,
            final Double maxSpeed,
            final Integer minCrewSize,
            final Integer maxCrewSize,
            final Double minRating,
            final Double maxRating
    ) {
        return entityManager.createQuery(createFilter(order, name, planet, shipType, after, before, isUsed, minSpeed, maxSpeed, minCrewSize, maxCrewSize, minRating, maxRating)
        ).getResultList();
    }

    private String createFilter(final String order,
                                final String name,
                                final String planet,
                                final ShipType shipType,
                                final Long after,
                                final Long before,
                                final Boolean isUsed,
                                final Double minSpeed,
                                final Double maxSpeed,
                                final Integer minCrewSize,
                                final Integer maxCrewSize,
                                final Double minRating,
                                final Double maxRating){
        String SQLshipType="";
        if(null != shipType){
            SQLshipType = " and ship.shipType = "+ "'" + shipType + "'";
        }
        String SQLisUsed = "";
        if(null != isUsed){
            SQLisUsed = " and ship.isUsed = " + isUsed;
        }

        final String SQLname = "ship.name like " + "'%" + name + "%'";
        final String SQLplanet=" and ship.planet like "+ "'%" + planet + "%'";
        final String SQLspeed = " and ship.speed >=" + minSpeed + " and ship.speed <= " + maxSpeed;
        final String SQLcrewSize = " and ship.crewSize >= " + minCrewSize+ " and ship.crewSize <= " + maxCrewSize;
        final String SQLrating = " and ship.rating >= " + minRating + " and ship.rating <= " + maxRating;

        final Date afterDate = new Date(after);
        final Date beforeDate = new Date(before);

        final String  SQLdateAfter = "'" + new SimpleDateFormat("yyyy-MM-dd").format(afterDate) + "'";
        final String SQLdateBefore = "'" + new SimpleDateFormat("yyyy-MM-dd").format(beforeDate) + "'";

        final String sort = " order by ship." + order + " asc";

        return "select ship from Ship ship" +
               " where " + SQLname +
               SQLplanet +
               SQLshipType +
               " and ship.prodDate BETWEEN " + SQLdateAfter + " and " + SQLdateBefore +
               SQLisUsed +
               SQLspeed +
               SQLcrewSize +
               SQLrating +
               sort;
    }
}
