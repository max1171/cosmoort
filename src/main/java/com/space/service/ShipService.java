package com.space.service;

import com.space.model.Ship;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Calendar;


public class ShipService {

    public double getRatingShip(Double speed, Boolean isUsed, Long prodDate){

        Calendar calendarActualThis = Calendar.getInstance();
        calendarActualThis.setTimeInMillis(33103209601119L);
        long actualDate = calendarActualThis.get(Calendar.YEAR);

        Calendar calendarThisProdDate = Calendar.getInstance();
        calendarThisProdDate.setTimeInMillis(prodDate);
        long thisProdDate = calendarThisProdDate.get(Calendar.YEAR);

        double KisUsed = isUsed ? 0.5 : 1.0;
        double rating = (80*speed*KisUsed)/(actualDate-thisProdDate + 1);

        MathContext mathContext = new MathContext(3, RoundingMode.HALF_DOWN );

        BigDecimal bigDecimal = new BigDecimal(rating,mathContext);

        return bigDecimal.doubleValue();
    }

    public Boolean verifyShip(Ship ship){
        if (null != ship.getName() && (ship.getName().isEmpty() || ship.getName().length() > 50)) {
            return false;
        }
        if (null != ship.getPlanet() && (ship.getPlanet().isEmpty() || ship.getPlanet().length() > 50)) {
            return false;
        }
        if (null != ship.getSpeed() && (ship.getSpeed() < 0.01 || ship.getSpeed() > 0.99)) {
            return false;
        }
        if (null != ship.getCrewSize() && (ship.getCrewSize() < 1 || ship.getCrewSize() > 9999)) {
            return false;
        }
        if (null != ship.getProdDate() && (ship.getProdDate().getTime() < 26192221200000L || ship.getProdDate().getTime() > 33134634000000L)) {
            return false;
        }
        return true;
    }
}
