package com.space.controller;

import com.space.model.Ship;
import com.space.model.ShipType;
import com.space.repository.ShipRepository;
import com.space.service.ShipService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Controller
@RequestMapping("/rest")
public class ShipController {
    private final ShipRepository shipRepository;
    private final ShipService shipService = new ShipService();

    @Autowired
    public ShipController(ShipRepository shipRepository) {
        this.shipRepository = shipRepository;
    }

    @GetMapping("/ships")
    public ResponseEntity<Iterable<Ship>> getShip(
            @RequestParam(name = "pageNumber", required = false, defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", required = false, defaultValue = "3") Integer pageSize,
            @RequestParam(name = "order", required = false, defaultValue = "ID") ShipOrder order,
            @RequestParam(name = "name", required = false, defaultValue = "") String name,
            @RequestParam(name = "planet", required = false, defaultValue = "") String planet,
            @RequestParam(name = "shipType", required = false, defaultValue = "") ShipType shipType,
            @RequestParam(name = "after", required = false, defaultValue = "26192246401119") Long after,
            @RequestParam(name = "before", required = false, defaultValue = "33134659201119") Long before,
            @RequestParam(name = "isUsed", required = false, defaultValue = "") Boolean isUsed,
            @RequestParam(name = "minSpeed", required = false, defaultValue = "0.01") Double minSpeed,
            @RequestParam(name = "maxSpeed", required = false, defaultValue = "0.99") Double maxSpeed,
            @RequestParam(name = "minCrewSize", required = false, defaultValue = "1") Integer minCrewSize,
            @RequestParam(name = "maxCrewSize", required = false, defaultValue = "9999") Integer maxCrewSize,
            @RequestParam(name = "minRating", required = false, defaultValue = "0") Double minRating,
            @RequestParam(name = "maxRating", required = false, defaultValue = "1000") Double maxRating
    ) {
        List<Ship> iterableShip = shipRepository.findFilterAndPaginationShip(
                pageNumber,
                pageSize,
                order.getFieldName(),
                name,
                planet,
                shipType,
                after,
                before,
                isUsed,
                minSpeed,
                maxSpeed,
                minCrewSize,
                maxCrewSize,
                minRating,
                maxRating
        );
        return ResponseEntity.ok(iterableShip);
    }

    @GetMapping("/ships/{id}")
    public ResponseEntity<Ship> getShipById(
            @PathVariable Long id
    ) {
        if (null == id || id <= 0) {
            return ResponseEntity.badRequest().build();
        }

        Ship ship = shipRepository.findById(id);
        if (null == ship) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(ship);
    }

    @GetMapping("/ships/count")
    public ResponseEntity<Integer> getShipCount(
            @RequestParam(name = "order", required = false, defaultValue = "ID") ShipOrder order,
            @RequestParam(name = "name", required = false, defaultValue = "") String name,
            @RequestParam(name = "planet", required = false, defaultValue = "") String planet,
            @RequestParam(name = "shipType", required = false, defaultValue = "") ShipType shipType,
            @RequestParam(name = "after", required = false, defaultValue = "26192246401119") Long after,
            @RequestParam(name = "before", required = false, defaultValue = "33134659201119") Long before,
            @RequestParam(name = "isUsed", required = false, defaultValue = "") Boolean isUsed,
            @RequestParam(name = "minSpeed", required = false, defaultValue = "0.01") Double minSpeed,
            @RequestParam(name = "maxSpeed", required = false, defaultValue = "0.99") Double maxSpeed,
            @RequestParam(name = "minCrewSize", required = false, defaultValue = "1") Integer minCrewSize,
            @RequestParam(name = "maxCrewSize", required = false, defaultValue = "9999") Integer maxCrewSize,
            @RequestParam(name = "minRating", required = false, defaultValue = "0") Double minRating,
            @RequestParam(name = "maxRating", required = false, defaultValue = "1000") Double maxRating
    ) {
        int count = shipRepository.findFilterShip(
                "id",
                name,
                planet,
                shipType,
                after,
                before,
                isUsed,
                minSpeed,
                maxSpeed,
                minCrewSize,
                maxCrewSize,
                minRating,
                maxRating
        ).size();
        return ResponseEntity.ok(count);
    }

    @PostMapping("/ships/{id}")
    public ResponseEntity<Ship> updateShip(@PathVariable final long id, @RequestBody final Ship newShip) {
        if (id <= 0) {
            return ResponseEntity.badRequest().build();
        }
        if (!shipService.verifyShip(newShip)) {
            return ResponseEntity.badRequest().build();
        }
        Ship ship = shipRepository.findById(id);
        if (null == ship) {
            return ResponseEntity.notFound().build();
        }
        if (newShip.getName() == null &&
            newShip.getProdDate() == null &&
            newShip.getSpeed() == null &&
            newShip.getShipType() == null &&
            newShip.getSpeed() == null &&
            newShip.getCrewSize() == null &&
            newShip.getPlanet() == null &&
            newShip.getRating() == null) {
            return ResponseEntity.ok(ship);
        }
        if (null != newShip.getName()) {
            ship.setName(newShip.getName());
        }
        if (null != newShip.getPlanet()) {
            ship.setPlanet(newShip.getPlanet());
        }
        if (null != newShip.getShipType()) {
            ship.setShipType(newShip.getShipType());
        }
        if (null != newShip.getProdDate()) {
            ship.setProdDate(newShip.getProdDate());
        }
        if (null != newShip.getUsed()) {
            ship.setUsed(newShip.getUsed());
        }
        if (null != newShip.getSpeed()) {
            ship.setSpeed(newShip.getSpeed());
        }
        if (null != newShip.getCrewSize()) {
            ship.setCrewSize(newShip.getCrewSize());
        }
        if (null != ship.getSpeed() && null != ship.getProdDate()) {
            double rating = shipService.getRatingShip(ship.getSpeed(), ship.getUsed(), ship.getProdDate().getTime());
            ship.setRating(rating);
        }
        return ResponseEntity.ok(shipRepository.save(ship));
    }

    @DeleteMapping("/ships/{id}")
    public ResponseEntity<Ship> deletShip(@PathVariable Long id) {
        if (null == id || id <= 0) {
            return ResponseEntity.badRequest().build();
        }
        if (null == shipRepository.findById(id)) {
            return ResponseEntity.notFound().build();
        }
        Ship ship = shipRepository.findById(id);
        shipRepository.delete(ship);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/ships")
    public ResponseEntity<Ship> createShip(@RequestBody Ship ship, Errors errors) {
        if (errors.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if(ship.getUsed() == null){
            ship.setUsed(false);
        }
        if(ship.getPlanet() == null || ship.getPlanet().isEmpty() || ship.getPlanet().length() > 50){
            return ResponseEntity.badRequest().build();
        }
        if(ship.getName() == null || ship.getName().isEmpty() || ship.getName().length() > 50){
            return ResponseEntity.badRequest().build();
        }
        if(ship.getCrewSize() == null || ship.getCrewSize() < 1 || ship.getCrewSize() > 9999){
            return ResponseEntity.badRequest().build();
        }
        if(ship.getSpeed() == null || ship.getSpeed() < 0.01 || ship.getSpeed() > 0.99){
            return ResponseEntity.badRequest().build();
        }
        if(ship.getShipType() == null){
            return ResponseEntity.badRequest().build();
        }
        if(ship.getProdDate() == null || ship.getProdDate().getTime() < 26192221200000L || ship.getProdDate().getTime() > 33134634000000L){
            return ResponseEntity.badRequest().build();
        }
        double rating = shipService.getRatingShip(ship.getSpeed(), ship.getUsed(), ship.getProdDate().getTime());
        ship.setRating(rating);
        return ResponseEntity.ok(shipRepository.save(ship));
    }
}
